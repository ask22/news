# == Schema Information
#
# Table name: grab_intervals
#
#  id            :bigint           not null, primary key
#  interval      :float            not null
#  name          :string           not null
#  next_start_at :datetime         not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_grab_intervals_on_interval  (interval) UNIQUE
#  index_grab_intervals_on_name      (name) UNIQUE
#
FactoryBot.define do
  factory :grab_interval do
    transient do
      next_start { Time.zone.now }
    end
    sequence(:name, 15) { |n| "#{n} minutes" }
    sequence(:interval, 15) { |n| 1.0 * n / (60 * 24) }
    next_start_at { next_start }
  end
end
