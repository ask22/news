# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string           not null
#  last_name              :string           not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  time_zone              :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
require 'rails_helper'

RSpec.describe User, type: :model do
  # Associations
  it { is_expected.to have_many :feeds }

  # Attributes
  it { expect(subject.attributes).to include('email') }
  it { expect(subject.attributes).to include('first_name') }
  it { expect(subject.attributes).to include('last_name') }
  it { expect(subject.attributes).to include('encrypted_password') }

  # Validations
  it { is_expected.to validate_presence_of :first_name }
  it { is_expected.to validate_presence_of :last_name }
  it { is_expected.to validate_presence_of :password }

  it { should_not allow_value('Ui ').for(:first_name) }
  it { should_not allow_value('Ivan.').for(:first_name) }
  it { should allow_value('2fL').for(:first_name) }

  it { should_not allow_value('Ui ').for(:last_name) }
  it { should_not allow_value('Ivan.').for(:last_name) }
  it { should allow_value('2fL').for(:first_name) }

  it { should_not allow_value('12345').for(:password) }
  it { should_not allow_value('aYd=ghjk').for(:password) }
  it { should_not allow_value('1asdfgH2').for(:password) }
  it { should allow_value('5asdfgH~').for(:password) }
end
