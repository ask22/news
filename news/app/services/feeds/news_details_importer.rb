# frozen_string_literal: true

module Feeds
  class NewsDetailsImporter < ApplicationService
    attr_reader :feed_id, :articles

    FIELDS_MAP = { 'author' => 'author', 'description' => 'description', 'published_at' => 'publishedAt',
                   'source_name' => 'source.name', 'title' => 'title', 'url' => 'url',
                   'url_to_image' => 'urlToImage' }.freeze

    def initialize(feed_id, articles)
      super()
      @feed_id = feed_id
      @articles = articles
    end

    def call
      return if articles.nil?

      feed_details = []
      iterate_articles do |params|
        feed_detail = FeedDetail.new(params)
        feed_detail.feed_id = feed_id
        feed_detail.validate ? feed_details << feed_detail : log_errors(feed_detail)
      end
      imp = FeedDetail.import feed_details, on_duplicate_key_ignore: true, validate: false
      log(imp)
      imp.ids.count
    end

    private

    def iterate_articles
      articles.each do |a|
        params = {}
        FIELDS_MAP.each { |key, value| params[key] = a.dig(*value.split('.')) }
        yield params
      end
    end

    def log(imp)
      Rails.logger.info "#{imp.ids.count} articles was imported, #{imp.failed_instances.count} failed objects"
    end

    def log_errors(feed)
      Rails.logger.error feed.inspect
      feed.errors.each do |e|
        Rails.logger.error e.inspect
      end
    end
  end
end
