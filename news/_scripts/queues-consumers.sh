# Check is RabbitMq started up
sh /opt/app/_scripts/check_rabbit.sh

cd /opt/app
env WORKERS=NewsWorker bundle exec rake sneakers:run &
env WORKERS=EmailWorker bundle exec rake sneakers:run