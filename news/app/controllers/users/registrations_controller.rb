# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  def update
    update_params = devise_parameter_sanitizer.sanitize(:account_update)
    @user = User.find(current_user.id)

    if needs_password?
      updated = @user.update_with_password(update_params)
    else
      update_params.delete('password')
      update_params.delete('password_confirmation')
      update_params.delete('current_password')
      @user.assign_attributes(update_params)
      updated = @user.save
    end

    if updated
      set_flash_message :notice, :updated
      redirect_to edit_user_registration_path
    else
      render :edit
    end
  end

  private

  def needs_password?
    @user.email != params[:user][:email] || params[:user][:password].present?
  end
end
