# frozen_string_literal: true

module Feeds
  class NewsGrabber < ApplicationService
    PAGE_SIZE = 30
    attr_reader :feed_id, :newsapi

    def initialize(feed_id)
      super()
      @feed_id = feed_id
      @newsapi = Newsapi::Utils.new ENV['NEWS_API_KEY']
    end

    def call
      ActiveRecord::Base.connection_pool.with_connection do
        return if feed.nil?

        update_feed_status(:runned_at)
        count = grab_news
        update_feed_status(:completed_at)
        notify_user(count)
      end
    end

    private

    def feed
      @feed ||= Feed.find(feed_id)
    rescue ActiveRecord::RecordNotFound
      puts "Feed with ID:#{feed_id} not found"
    end

    def update_feed_status(field)
      feed.update(field => Time.zone.now)
    end

    def params(page)
      {
        q: feed.search_query,
        language: feed.language,
        sortBy: 'relevancy',
        pageSize: PAGE_SIZE,
        page: page,
        from: from_date
      }
    end

    def grab_news(page = 1)
      data = newsapi.get_data(**params(page))
      return 0 unless status_ok?(data)

      count = Feeds::NewsDetailsImporter.call(feed_id, data['articles'])
      last_page?(data, page) ? count : grab_news(page + 1) + count
    rescue StandardError => e
      handle_unexpected_error(e)
      0
    end

    def last_page?(data, page)
      data['totalResults'] <= page * PAGE_SIZE
    end

    def from_date
      (Time.zone.now - 1.day).strftime('%Y-%m-%d')
    end

    def status_ok?(data)
      return true if data['status'] == 'ok'

      handle_invalid_response(data)
    end

    def handle_invalid_response(data)
      Rails.logger.error data['message']
    end

    def handle_unexpected_error(err)
      Rails.logger.error err.inspect
    end

    def notify_user(count)
      return unless feed.is_email_on_update && count.positive?

      data = { 'email' => feed.user.email, 'name' => feed.name, 'updated_at' => feed.completed_at, 'count' => count }
      Rabbitmq::Publisher.push(data.to_json, 'emails')
    end
  end
end
