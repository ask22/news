class FixFeedsColumnName < ActiveRecord::Migration[6.1]
  def change
    rename_column :feeds, :run_at, :runned_at
  end
end
