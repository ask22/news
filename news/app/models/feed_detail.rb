# frozen_string_literal: true

# == Schema Information
#
# Table name: feed_details
#
#  id           :bigint           not null, primary key
#  author       :string
#  description  :string           not null
#  published_at :datetime         not null
#  source_name  :string           not null
#  title        :string           not null
#  url          :string           not null
#  url_to_image :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  feed_id      :integer          not null
#
# Indexes
#
#  index_feed_details_on_feed_id          (feed_id)
#  index_feed_details_on_feed_id_and_url  (feed_id,url) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (feed_id => feeds.id) ON DELETE => cascade
#
class FeedDetail < ApplicationRecord
  belongs_to :feed

  validates :description, presence: true
  validates :published_at, presence: true
  validates :source_name, presence: true
  validates :title, presence: true
  validates :url, presence: true
  validates :feed, presence: true
end
