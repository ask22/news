class ChangeColumnAuthorInFeedDetails < ActiveRecord::Migration[6.1]
  def change
    change_column_null :feed_details, :author, true
    add_index :feed_details, [:feed_id, :url], unique: true
  end
end
