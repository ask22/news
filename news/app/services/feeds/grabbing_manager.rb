# frozen_string_literal: true

module Feeds
  class GrabbingManager < ApplicationService
    def call
      return if ready_feeds.empty?

      ready_feeds.each do |feed|
        Feeds::EnqueueFeed.call(feed.id)
      end
      update_next_start
      Rabbitmq::Publisher.connection.close
    end

    private

    def ready_intervals
      @ready_intervals ||= GrabInterval.where('next_start_at < ?', Time.zone.now)
    end

    def ready_feeds
      @ready_feeds ||= Feed.where('grab_interval_id IN (?)', ready_intervals.map(&:id))
    end

    def update_next_start
      @ready_intervals.each { |i| i.update(next_start_at: Time.zone.now + i.interval.day) }
    end
  end
end
