host="rabbitmq"
port="15672"

>&2 echo "!!!!!!!! Check $host for available !!!!!!!!"

until curl -o /dev/null http://"$host":"$port"; do
  >&2 echo "Container $host is unavailable - sleeping"
  sleep 5
done

>&2 echo "Container $host is up!"
