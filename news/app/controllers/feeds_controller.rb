# frozen_string_literal: true

class FeedsController < ApplicationController
  before_action :authenticate_user!
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  Language = Struct.new(:key, :label)

  def index
    feeds
  end

  def show
    @feed_details = feed.feed_details.order('published_at DESC').page(params[:page])
  end

  def new
    @feed = Feed.new
  end

  def create
    @feed = Feed.new(feed_params)
    @feed.user = current_user

    if @feed.save
      Feeds::EnqueueFeed.call(@feed.id)
      redirect_to feeds_path
    else
      render :new
    end
  end

  def destroy
    feed.destroy

    redirect_to feeds_path
  end

  def refresh
    Feeds::EnqueueFeed.call(params[:id])
    flash[:notice] = I18n.t('feeds.refresh.start')
    redirect_to feed_path
  end

  private

  def feed
    @feed ||= Feed.find(params[:id]).decorate
  end

  def feeds
    @feeds ||= current_user.feeds.all.order('created_at DESC').decorate
  end

  def feed_params
    params.require(:feed).permit(:name, :language, :search_query, :grab_interval_id, :is_email_on_update)
  end

  def record_not_found(error)
    flash.now.alert = error.message

    render 'shared/_alert'
  end
end
