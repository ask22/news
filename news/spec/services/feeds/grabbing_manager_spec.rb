require 'rails_helper'

RSpec.describe Feeds::GrabbingManager do
  let!(:ready_interval) { FactoryBot.create(:grab_interval) }
  let!(:unready_interval) { FactoryBot.create(:grab_interval, next_start: Time.zone.now + 1.day) }
  let!(:ready_feeds) { FactoryBot.create_list(:feed, 5, grab_interval: ready_interval) }
  let!(:unready_feeds) { FactoryBot.create_list(:feed, 3, grab_interval: unready_interval) }

  before do
    allow(Feeds::EnqueueFeed).to receive(:new) { -> { true } }
    Feeds::GrabbingManager.call
  end

  it 'should update next_start time for ready intervals' do
    start_at = GrabInterval.find(ready_interval.id).next_start_at
    expect(start_at).to be > Time.zone.now
  end

  it 'should not update next_start time for unready intervals' do
    start_at = GrabInterval.find(unready_interval.id).next_start_at.round
    expect(start_at).to eq unready_interval.next_start_at.round
  end
end
