# frozen_string_literal: true

namespace :rabbitmq do
  desc 'Setup routing'
  task :setup do
    require 'bunny'

    conn = Bunny.new ENV['RABBITMQ_URL']
    conn.start

    ch = conn.create_channel

    x = ch.direct('news.exchange')
    ch.queue('news.feeder', { durable: true, auto_delete: false }).bind(x, routing_key: 'fetch_news')
    ch.queue('news.mailer', { durable: true, auto_delete: false }).bind(x, routing_key: 'emails')

    conn.close
  end
end

desc 'Update feeds task'
task update_feeds: :environment do
  Rails.logger.info '-------------------- Grabbing cron task is running ---------------------'
  Feeds::GrabbingManager.call
end
