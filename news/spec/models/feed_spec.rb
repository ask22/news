# == Schema Information
#
# Table name: feeds
#
#  id                 :bigint           not null, primary key
#  completed_at       :datetime
#  is_email_on_update :boolean
#  language           :string           default("")
#  name               :string           not null
#  runned_at          :datetime
#  search_query       :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  grab_interval_id   :integer          not null
#  user_id            :integer          not null
#
# Indexes
#
#  index_feeds_on_created_at        (created_at)
#  index_feeds_on_grab_interval_id  (grab_interval_id)
#  index_feeds_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (grab_interval_id => grab_intervals.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Feed, type: :model do
  # Associations
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :grab_interval }
  it { is_expected.to have_many :feed_details }

  # Columns
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:search_query).of_type(:string) }
  it { is_expected.to have_db_column(:language).of_type(:string) }
  it { is_expected.to have_db_column(:is_email_on_update).of_type(:boolean) }

  # Validations
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :search_query }
  it { is_expected.to validate_presence_of :grab_interval }
  it { is_expected.to validate_presence_of :user }
end
