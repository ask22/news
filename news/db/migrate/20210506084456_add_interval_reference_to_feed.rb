class AddIntervalReferenceToFeed < ActiveRecord::Migration[6.1]
  def change
    add_column :feeds, :grab_interval_id, :integer, null: false
    remove_column :feeds, :update_interval, :float, null: false

    add_foreign_key :feeds, :grab_intervals, on_delete: :restrict
    add_index :feeds, :grab_interval_id
  end
end
