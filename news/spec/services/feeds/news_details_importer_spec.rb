require 'rails_helper'

RSpec.describe Feeds::NewsDetailsImporter, type: :controller do
  let(:feed) { FactoryBot.create(:feed, :notify) }
  let(:articles) { JSON.parse(file_fixture('articles.json').read)['articles'] }

  before do
    @count = Feeds::NewsDetailsImporter.call(feed.id, articles)
  end

  it 'should return correct Articles count' do
    expect(@count).to eq 5
  end

  it 'should store all Articles to DB' do
    stored_articles = FeedDetail.where(feed_id: feed.id).
      select(:source_name, :author, :title, :description, :url, :url_to_image, :published_at).
      map { |i| i.attributes.symbolize_keys }

    expected_articles = articles.map do |a|
      {
        id: nil, source_name: a['source']['name'], author: a['author'], title: a['title'],
        description: a['description'], url: a['url'], url_to_image: a['urlToImage'], published_at: a['publishedAt']
      }
    end

    expect(stored_articles).to eq expected_articles
  end

  it 'should not duplicate news in DB' do
    again_load_count =  Feeds::NewsDetailsImporter.call(feed.id, articles)
    expect(again_load_count).to eq 0
  end
end
