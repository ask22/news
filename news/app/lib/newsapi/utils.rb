# frozen_string_literal: true

module Newsapi
  class Utils
    VERSION = 'v2'
    BASE_URL = "https://newsapi.org/#{VERSION}/everything"

    attr_reader :api_key

    def initialize(api_key)
      @api_key = api_key
    end

    def get_data(**args)
      uri = URI(make_url(**args))
      req = Net::HTTP::Get.new(uri)
      res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
      JSON.parse(res.body)
    rescue StandardError => e
      { 'status' => 'error', 'message' => e.message }
    end

    private

    def make_url(**params)
      url = "#{BASE_URL}?"
      params.each { |key, value| url += "#{key}=#{value}&" }
      url += "apiKey=#{api_key}"
    end
  end
end
