# Check is RabbitMq started up
sh /opt/app/_scripts/check_rabbit.sh

# Ensure the log file exists
touch /opt/app/log/crontab.log

# Copy env for cronjob
env > /etc/crontab

# Added a cronjob in a new crontab
echo "* * * * * cd /opt/app && RAILS_ENV=development /usr/local/bin/bundle exec rake update_feeds >> /opt/app/log/crontab.log 2>&1" >> /etc/crontab

# Registering the new crontab
crontab /etc/crontab

# Starting the cron
/usr/sbin/service cron start

# Displaying logs
# Useful when executing docker-compose logs mycron
tail -f /opt/app/log/crontab.log
