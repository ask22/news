require 'rails_helper'

RSpec.describe FeedsController, type: :controller do
  let(:feed) { FactoryBot.create(:feed, :notify) }
  let(:user) { feed.user }
  let(:params) { Hash[:params, { id: feed.id }] }

  before { sign_in(user) }

  describe '#index' do
    it 'should render Feeds index page' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe '#show' do
    it 'should render Feed Detail page' do
      get :show, params
      expect(response).to render_template(:show)
    end
  end

  describe '#new' do
    it 'should render New Feed page' do
      get :new, params
      expect(response).to render_template(:new)
    end
  end

  describe '#create' do
    before do
      grab_interval_id = FactoryBot.create(:grab_interval).id
      @params = { 'feed' => {
        'name' => 'New Feed',
        'language' => 'en',
        'search_query' => 'news',
        'grab_interval_id' => grab_interval_id
      }}

      post :create, params: @params
    end

    it 'should create new Feed' do
      created_feed = Feed.find_by(@params['feed'])
      expect(created_feed).to be_truthy
    end

    it 'should be redirected to the Feeds page' do
      expect(response).to redirect_to(feeds_path)
    end
  end

  describe '#destroy' do
    before { delete :destroy, params }

    it 'should be redirected to the Feeds page' do
      expect(response).to redirect_to(feeds_path)
    end

    it 'should delete selected Feed' do
      deleted_feed = Feed.where(id: feed.id)
      expect(deleted_feed.empty?).to be_truthy
    end
  end

  describe '#refresh' do
    it 'should render Feed Detail page' do
      post :refresh, params
      expect(response).to redirect_to(feed_path)
    end
  end
end
