user = User.create_with(password: '1Qwerty&', password_confirmation: '1Qwerty&').
  find_or_create_by(email: 'ok@mail.ru') do |user|
  user.first_name = 'Mike'
  user.last_name = 'Dow'
  user.confirmed_at = Time.zone.now
end

gi = GrabInterval.find_or_create_by(name: '15 minutes') do |i|
  i.interval = 1.0/96
  i.next_start_at = Time.zone.now
end
gi.feeds << Feed.new(language: 'en', name: 'ruby news', search_query: 'ruby', user_id: user.id)
gi.feeds << Feed.new(language: 'ru', name: 'php news', search_query: 'php', user_id: user.id, is_email_on_update: true)

gi = GrabInterval.find_or_create_by(name: '1 hour') do |i|
  i.interval = 1.0/24
  i.next_start_at = Time.zone.now + 60
end
gi.feeds << Feed.new(language: 'en', name: 'oracle news', search_query: 'oracle', user_id: user.id, is_email_on_update: true)
gi.feeds << Feed.new(language: 'en', name: 'postgresql news', search_query: 'postgresql', user_id: user.id)

gi = GrabInterval.find_or_create_by(name: '3 hours') do |i|
  i.interval = 1.0/8
  i.next_start_at = Time.zone.now + 120
end
gi.feeds << Feed.new(language: 'en', name: 'banana news', search_query: 'banana', user_id: user.id, is_email_on_update: true)
gi.feeds << Feed.new(language: 'en', name: 'orange news', search_query: 'orange', user_id: user.id, is_email_on_update: true)

gi = GrabInterval.find_or_create_by(name: '12 hours') do |i|
  i.interval = 1.0/2
  i.next_start_at = Time.zone.now + 180
end
gi.feeds << Feed.new(language: 'en', name: 'the U.S. news', search_query: 'US', user_id: user.id, is_email_on_update: true)
gi.feeds << Feed.new(language: 'en', name: 'Russian news', search_query: 'russia', user_id: user.id)

gi = GrabInterval.find_or_create_by(name: '24 hours') do |i|
  i.interval = 1.0
  i.next_start_at = Time.zone.now + 240
end
gi.feeds << Feed.new(language: 'en', name: 'weather news', search_query: 'weather', user_id: user.id, is_email_on_update: true)
gi.feeds << Feed.new(language: 'en', name: 'climate news', search_query: 'climate', user_id: user.id, is_email_on_update: true)