# frozen_string_literal: true

class NewsWorker
  include Sneakers::Worker
  from_queue 'news.feeder', env: nil

  def work(feed_id)
    Rails.logger.info '------- News grabbing service was called -------'
    Feeds::NewsGrabber.call(feed_id)
    ack!
  end
end
