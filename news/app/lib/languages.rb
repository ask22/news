class Languages
  AVAILABLE_LANGUAGES = %w[ar de en es fr he it nl no pt ru se ud zh].freeze

  def self.caption(key)
    I18nData.languages(I18n.locale)[key.upcase] ||= key
  end

  def self.dataset
    AVAILABLE_LANGUAGES.map do |key|
      OpenStruct.new(key: key, label: Languages.caption(key))
    end
  end
end