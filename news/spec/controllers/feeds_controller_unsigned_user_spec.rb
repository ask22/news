require 'rails_helper'

RSpec.describe FeedsController, type: :controller do
  let(:params) { Hash[:params, { id: 0 }] }

  describe '#index' do
    it 'should redirect to login page' do
      get :index
      expect(response).to redirect_to(user_session_path)
    end
  end

  describe '#show' do
    it 'should redirect to login page' do
      get :show, params
      expect(response).to redirect_to(user_session_path)
    end
  end

  describe '#new' do
    it 'should redirect to login page' do
      get :new
      expect(response).to redirect_to(user_session_path)
    end
  end

  describe '#create' do
    it 'should redirect to login page' do
      post :create
      expect(response).to redirect_to(user_session_path)
    end
  end

  describe '#destroy' do
    it 'should redirect to login page' do
      post :destroy, params
      expect(response).to redirect_to(user_session_path)
    end
  end

  describe '#refresh' do
    it 'should redirect to login page' do
      post :refresh, params
      expect(response).to redirect_to(user_session_path)
    end
  end
end
