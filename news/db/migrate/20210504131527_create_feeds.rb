class CreateFeeds < ActiveRecord::Migration[6.1]
  def change
    create_table :feeds do |t|
      t.integer :user_id, null: false
      t.string :name, null: false
      t.string :search_query, null: false
      t.string :language, default: ""
      t.float :update_interval, null: false
      t.boolean :is_email_on_update

      t.timestamps
    end

    add_foreign_key :feeds, :users
    add_index :feeds, :user_id
    add_index :feeds, :created_at, order: {created_at: :desc}
  end
end
