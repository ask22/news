class CreateGrabIntervals < ActiveRecord::Migration[6.1]
  def change
    create_table :grab_intervals do |t|
      t.float :interval, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_index :grab_intervals, :interval, unique: true
    add_index :grab_intervals, :name, unique: true
  end
end
