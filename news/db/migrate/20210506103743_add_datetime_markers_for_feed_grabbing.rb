class AddDatetimeMarkersForFeedGrabbing < ActiveRecord::Migration[6.1]
  def change
    add_column :feeds, :run_at, :datetime
    add_column :feeds, :completed_at, :datetime
  end
end
