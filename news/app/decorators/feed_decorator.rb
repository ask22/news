class FeedDecorator < ApplicationDecorator
  delegate_all

  def language_caption
    Languages.caption(object.language)
  end
end
