# frozen_string_literal: true

class EmailWorker
  include Sneakers::Worker
  from_queue 'news.mailer', env: nil

  def work(data)
    Rails.logger.info ' ---------------------------------  EMAIL  ----------------------------------'
    NewsMailer.notify(data).deliver
    ack!
  end
end
