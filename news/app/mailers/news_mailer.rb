# frozen_string_literal: true

class NewsMailer < ApplicationMailer
  def notify(message)
    data = JSON.parse(message)
    @name = data['name']
    @updated_at = data['updated_at']
    @count = data['count']
    mail(to: data['email'], subject: 'News by feed was updated')
  end
end
