# Dockerfile development version
FROM ruby:2.7.2 AS news-project

ARG USER_ID
ARG GROUP_ID
ARG RAILS_ENV

RUN addgroup --gid $GROUP_ID user
RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user
RUN useradd -m --no-log-init --system $USER_ID -g sudo

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg -o /root/yarn-pubkey.gpg && apt-key add /root/yarn-pubkey.gpg
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y --no-install-recommends nodejs yarn cron

ENV INSTALL_PATH /opt/app
RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH
COPY news/ .
RUN rm -rf node_modules vendor
RUN gem install rails
RUN if [ "$RAILS_ENV" = "production" ] ; then bundle install --without development test ; else bundle install ; fi
RUN yarn install
RUN chown -R user:user /opt/app

# USER $USER_ID

CMD ["sh", "/opt/app/_scripts/web.sh"]
