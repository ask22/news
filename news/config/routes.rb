Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }

  authenticated :user do
    root 'feeds#index', as: :authenticated_root
  end

  root 'feeds#index'

  resources :feeds, except: [:edit, :update], constraints: { id: /\d+/ }
  post 'feeds/:id/refresh', to: 'feeds#refresh', as: 'refresh_feed', constraints: { id: /\d+/ }
end
