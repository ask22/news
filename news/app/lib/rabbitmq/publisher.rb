# frozen_string_literal: true

module Rabbitmq
  class Publisher
    def self.push(message, routing_key)
      x = channel.direct('news.exchange')
      x.publish(message, timestamp: Time.now.to_i, routing_key: routing_key)
    end

    def self.channel
      @channel ||= connection.create_channel
    end

    def self.connection
      @connection ||= Bunny.new(ENV['RABBITMQ_URL']).tap(&:start)
    end
  end
end
