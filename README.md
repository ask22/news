# Personal News Feed

## Requirements

- Docker
- Docker Compose

## Installation
```
clone repository
create .env file in the root project directory and set variables (see example.env)
docker-compose build [--build-arg RAILS_ENV=production]
docker-compose run news rails db:create
docker-compose run news rails db:migrate
docker-compose run news rails db:seed
docker-compose run --user "$(id -u):$(id -g)" news rails webpacker:install
```

## Run application
```
docker-compose up
```

## Configuration
- All variables pass through .env-file (see example.env)

## How to run the test suite

`docker-compose run rails spec`