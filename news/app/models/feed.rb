# frozen_string_literal: true

# == Schema Information
#
# Table name: feeds
#
#  id                 :bigint           not null, primary key
#  completed_at       :datetime
#  is_email_on_update :boolean
#  language           :string           default("")
#  name               :string           not null
#  runned_at          :datetime
#  search_query       :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  grab_interval_id   :integer          not null
#  user_id            :integer          not null
#
# Indexes
#
#  index_feeds_on_created_at        (created_at)
#  index_feeds_on_grab_interval_id  (grab_interval_id)
#  index_feeds_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (grab_interval_id => grab_intervals.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id)
#
class Feed < ApplicationRecord
  belongs_to :user
  belongs_to :grab_interval
  has_many :feed_details, dependent: :destroy

  validates :name, presence: true
  validates :search_query, presence: true, length: { maximum: 60 }
  validates :grab_interval, presence: true
  validates :user, presence: true
end
