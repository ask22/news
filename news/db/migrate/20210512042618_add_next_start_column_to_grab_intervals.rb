class AddNextStartColumnToGrabIntervals < ActiveRecord::Migration[6.1]
  def change
    add_column :grab_intervals, :next_start_at, :datetime, null: false
  end
end
