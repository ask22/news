# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  around_action :switch_time_zone, if: :current_user

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[first_name last_name time_zone])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[first_name last_name time_zone])
  end

  def switch_time_zone(&block)
    Time.use_zone(current_user.time_zone, &block)
  end
end
