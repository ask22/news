class CreateFeedDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :feed_details do |t|
      t.integer :feed_id, null: false
      t.string :title, null: false
      t.string :url, null: false
      t.string :url_to_image
      t.string :description, null: false
      t.string :author, null: false
      t.datetime :published_at, null: false
      t.string :source_name, null: false

      t.timestamps
    end

    add_foreign_key :feed_details, :feeds, on_delete: :cascade
    add_index :feed_details, :feed_id
  end
end
