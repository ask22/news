# frozen_string_literal: true

class EmailValidator < ActiveModel::Validator
  def validate(record)
    return if record.email.match? URI::MailTo::EMAIL_REGEXP

    record.errors[:email] << I18n.t('errors.messages.email.invalid')
  end
end
