# == Schema Information
#
# Table name: grab_intervals
#
#  id            :bigint           not null, primary key
#  interval      :float            not null
#  name          :string           not null
#  next_start_at :datetime         not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_grab_intervals_on_interval  (interval) UNIQUE
#  index_grab_intervals_on_name      (name) UNIQUE
#
require 'rails_helper'

RSpec.describe GrabInterval, type: :model do
  # Associations
  it { is_expected.to have_many :feeds }

  # Columns
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:interval).of_type(:float) }

  # Validations
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :interval }
end
