# == Schema Information
#
# Table name: feed_details
#
#  id           :bigint           not null, primary key
#  author       :string
#  description  :string           not null
#  published_at :datetime         not null
#  source_name  :string           not null
#  title        :string           not null
#  url          :string           not null
#  url_to_image :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  feed_id      :integer          not null
#
# Indexes
#
#  index_feed_details_on_feed_id          (feed_id)
#  index_feed_details_on_feed_id_and_url  (feed_id,url) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (feed_id => feeds.id) ON DELETE => cascade
#
require 'rails_helper'

RSpec.describe FeedDetail, type: :model do
  # Associations
  it { is_expected.to belong_to :feed }

  # Columns
  it { is_expected.to have_db_column(:author).of_type(:string) }
  it { is_expected.to have_db_column(:description).of_type(:string) }
  it { is_expected.to have_db_column(:published_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:source_name).of_type(:string) }
  it { is_expected.to have_db_column(:title).of_type(:string) }
  it { is_expected.to have_db_column(:url).of_type(:string) }
  it { is_expected.to have_db_column(:url_to_image).of_type(:string) }

  # Validations
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :published_at }
  it { is_expected.to validate_presence_of :source_name }
  it { is_expected.to validate_presence_of :title }
  it { is_expected.to validate_presence_of :url }
end
