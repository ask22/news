# frozen_string_literal: true

# == Schema Information
#
# Table name: grab_intervals
#
#  id            :bigint           not null, primary key
#  interval      :float            not null
#  name          :string           not null
#  next_start_at :datetime         not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_grab_intervals_on_interval  (interval) UNIQUE
#  index_grab_intervals_on_name      (name) UNIQUE
#
class GrabInterval < ApplicationRecord
  has_many :feeds

  validates :interval, presence: true
  validates :name, presence: true, length: { maximum: 20 }
end
