# frozen_string_literal: true

module Feeds
  class EnqueueFeed < ApplicationService
    attr_reader :id

    def initialize(id)
      super()
      @id = id
    end

    def call
      Rabbitmq::Publisher.push(id.to_s, 'fetch_news')
    end
  end
end
